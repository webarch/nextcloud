# Webarchitects Nextcloud Ansible Role

This repo contains an Ansible Galaxy role to install Nextcloud, see the [defaults/main.yml](defaults/main.yml).

For new installs the [latest Nextcloud version](https://github.com/nextcloud/server/wiki/Maintenance-and-Release-Schedule) will be installed and for existing installs outstanding upgrades will be applied, when `nextcloud_autoupdate` is set to true, if multiple upgrades are required then the role needs to be run multiple times.

This role imports the [ClamAV](https://git.coop/webarch/clamav) and [Coturn](https://git.coop/webarch/coturn) roles for the anti-virus app and Nextcloud Talk if these are set to be installed (they are not by default).

This role can be used with the [ONLYOFFICE Document Server role](https://git.coop/webarch/onlyoffice), see the [Nextcloud development server repo](https://git.coop/webarch/nextcloud-server) for an example of how this works.

Note that this role has only been tested based on the assumption that the user running Nextcloud (either via Apache and `mod_php` and `mod_itk` or via a PHP FPM pool) and the database user are the same, eg `nextcloud`, some work might be needed to enable it to be used to run Nextcloud as `www-data`.

## Nextcloud configuration

Get the existing config as YAML:

```bash
occ config:list --private | jq | yq -o=yaml -P
```

Add a admin user:

```bash
occ user:add --display-name="Webarchitects Admin" webarch
occ user:setting webarch settings email "chris@webarchitects.co.uk"
occ group:adduser admin webarch
```

## Full text search

the [Elasticsearch role](https://git.coop/webarch/elasticsearch) can be used to install Elasticsearch, then these apps need to be installed:

- [fulltextsearch](https://apps.nextcloud.com/apps/fulltextsearch)
- [files_fulltextsearch](https://github.com/nextcloud/files_fulltextsearch)
- [fulltextsearch_elasticsearch](https://apps.nextcloud.com/apps/fulltextsearch_elasticsearch)

This role will configure the Elasticsearch login and Systemd to keep the index updated but it should be manually created to start with, stop the systemd unit as root:

```bash
service nextcloud-fulltext-elasticsearch-worker stop
```

Then as the Nextcloud user manually create the initial index:

```bash
occ fulltextsearch:stop
occ fulltextsearch:index
```

Then restart the systemd unit file.

For debugging Elasticsearch, get a list of indexes:

```bash
curl http://localhost:9200/_cat/indices
```

## Repo History

The [Ansible Nextcloud project](https://git.coop/webarch/nextcloud-server) that was at `https://git.coop/webarch/nextcloud` has moved to [git.coop/webarch/nextcloud-server](https://git.coop/webarch/nextcloud-server).

## Copyright

Copyright 2019-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
