# Copyright 2019-2025 Chris Croome
#
# This file is part of the Webarchitects Nextcloud Ansible role.
#
# The Webarchitects Nextcloud Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Nextcloud Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Nextcloud Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Check remote ONLYOFICE Document Server
  block:

    - name: Check if the ONLYOFFICE Document Server local.json file exists on the ONLYOFFICE server
      ansible.builtin.stat:
        path: /etc/onlyoffice/documentserver/local.json
      register: nextcloud_onlyoffice_local_json_file
      delegate_to: "{{ nextcloud_onlyoffice_documentserver }}"

    - name: Read the ONLYOFFICE Document Server local.json file
      block:

        - name: Slurp the ONLYOFFICE Document Server local.json file
          ansible.builtin.slurp:
            src: /etc/onlyoffice/documentserver/local.json
          register: nextcloud_onlyoffice_local_json_b64encoded
          delegate_to: "{{ nextcloud_onlyoffice_documentserver }}"
          no_log: "{% if ansible_verbosity == '0' %}true{% else %}false{% endif %}"

        - name: Set a fact for the ONLYOFFICE Document Server local.json
          ansible.builtin.set_fact:
            nextcloud_onlyoffice_local_json: "{{ nextcloud_onlyoffice_local_json_b64encoded['content'] | b64decode | from_json }}"
          no_log: "{% if ansible_verbosity == '0' %}true{% else %}false{% endif %}"

        - name: Debug ONLYOFFICE Document Server local.json
          ansible.builtin.debug:
            var: nextcloud_onlyoffice_local_json
            verbosity: 3

        - name: Set a fact for the ONLYOFFICE services.CoAuthoring.secret.session.string
          ansible.builtin.set_fact:
            nextcloud_onlyoffice_jwt_secret: "{{ nextcloud_onlyoffice_local_json.services.CoAuthoring.secret.session.string }}"
          no_log: "{% if ansible_verbosity == '0' %}true{% else %}false{% endif %}"
          when:
            - nextcloud_onlyoffice_local_json.services.CoAuthoring.secret.session.string is defined
            - nextcloud_onlyoffice_local_json.services.CoAuthoring.secret.session.string | length > 0

        - name: Set a fact for the ONLYOFFICE services.CoAuthoring.token.outbox.header
          ansible.builtin.set_fact:
            nextcloud_onlyoffice_jwt_header: "{{ nextcloud_onlyoffice_local_json.services.CoAuthoring.token.outbox.header }}"
          no_log: "{% if ansible_verbosity == '0' %}true{% else %}false{% endif %}"
          when:
            - nextcloud_onlyoffice_local_json.services.CoAuthoring.token.outbox.header  is defined
            - nextcloud_onlyoffice_local_json.services.CoAuthoring.token.outbox.header  | length > 0

      when: nextcloud_onlyoffice_local_json_file.stat.exists | bool

  tags:
    - nextcloud
...
