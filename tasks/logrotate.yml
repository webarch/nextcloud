# Copyright 2019-2025 Chris Croome
#
# This file is part of the Webarchitects Nextcloud Ansible role.
#
# The Webarchitects Nextcloud Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Nextcloud Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Nextcloud Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Nextcloud logrotate
  block:

    - name: "Nextcloud logrotate absent for {{ nextcloud_user }}"
      ansible.builtin.file:
        path: "/etc/logrotate.d/nextcloud_{{ nextcloud_user }}"
        state: absent
      when: not nextcloud_logrotate | bool

    - name: Nextcloud logrotate present
      block:

        - name: "Nextcloud logrotate config present for {{ nextcloud_user }}"
          ansible.builtin.template:
            src: logrotate.j2
            dest: "/etc/logrotate.d/nextcloud_{{ nextcloud_user }}"
            owner: root
            group: root
            mode: "0644"

        - name: "Stat Nextcloud logrotate config present for {{ nextcloud_user }}"
          ansible.builtin.stat:
            path: "/etc/logrotate.d/nextcloud_{{ nextcloud_user }}"
          register: nextcloud_logrotate_path

        - name: Check and run Nextcloud logrotate
          block:

            - name: "Check Nextcloud logrotate for {{ nextcloud_user }}"
              ansible.builtin.command: "/usr/sbin/logrotate -d /etc/logrotate.d/nextcloud_{{ nextcloud_user }}"
              check_mode: false
              changed_when: false

            - name: "Run Nextcloud logrotate for {{ nextcloud_user }}"
              ansible.builtin.command: "/usr/sbin/logrotate -f /etc/logrotate.d/nextcloud_{{ nextcloud_user }}"
              register: nextcloud_logrotate_run
              changed_when:
                - nextcloud_logrotate_run.stdout_lines | length > 0
                - ( "skipping rotation" not in nextcloud_logrotate_run.stderr )
              failed_when:
                - nextcloud_logrotate_run.rc != 0
                - ( "skipping rotation" not in nextcloud_logrotate_run.stderr )

          when: nextcloud_logrotate_path.stat.exists | bool

      when: nextcloud_logrotate | bool

  when: nextcloud_logrotate is defined
  tags:
    - nextcloud
...
