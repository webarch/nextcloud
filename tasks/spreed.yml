# Copyright 2019-2025 Chris Croome
#
# This file is part of the Webarchitects Nextcloud Ansible role.
#
# The Webarchitects Nextcloud Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Nextcloud Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Nextcloud Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Configure spreed, aka Nextcloud Talk
  block:

    - name: Check if /root/.coturn.auth-secret exists
      ansible.builtin.stat:
        path: /root/.coturn.auth-secret
      register: nextcloud_coturn_auth_secret_file

    - name: Fail if /root/.coturn.auth-secret doesn't exist
      ansible.builtin.fail:
        msg: "/root/.coturn.auth-secret should be created by the Coturn role https://git.coop/webarch/coturn"
      when: not nextcloud_coturn_auth_secret_file.stat.exists

    - name: Slurp the TURN authorization secret
      ansible.builtin.slurp:
        src: /root/.coturn.auth-secret
      register: nextcloud_coturn_auth_secret_b64encoded

    - name: Set a fact for the TURN authorization secret
      ansible.builtin.set_fact:
        nextcloud_coturn_auth_secret: "{{ nextcloud_coturn_auth_secret_b64encoded['content'] | b64decode | trim }}"

    # This is being done using a template due to the elaborate escaping needed
    - name: Generate a file containing the TURN server settings
      ansible.builtin.template:
        src: spreed_turn_servers.j2
        dest: /root/.spreed.turn_servers
        owner: root
        group: root
        mode: "0600"

    - name: Slurp the TURN server settings
      ansible.builtin.slurp:
        src: /root/.spreed.turn_servers
      register: nextcloud_spreed_turn_servers_b64encoded

    - name: Set a fact for the spreed TURN server
      ansible.builtin.set_fact:
        nextcloud_spreed_turn_servers: "{{ nextcloud_spreed_turn_servers_b64encoded['content'] | b64decode | trim }}"

    - name: Set the spreed turn_servers configuration
      ansible.builtin.command: '{{ nextcloud_php_alias }} occ config:app:set spreed turn_servers --value="{{ nextcloud_spreed_turn_servers }}"'
      args:
        chdir: "{{ nextcloud_docroot }}"
      become: true
      become_user: "{{ nextcloud_user }}"

    - name: Generate a file containing the STUN server settings
      ansible.builtin.template:
        src: spreed_stun_servers.j2
        dest: /root/.spreed.stun_servers
        owner: root
        group: root
        mode: "0600"

    - name: Slurp the STUN server settings
      ansible.builtin.slurp:
        src: /root/.spreed.stun_servers
      register: nextcloud_spreed_stun_servers_b64encoded

    - name: Set a fact for the spreed STUN servers
      ansible.builtin.set_fact:
        nextcloud_spreed_stun_servers: "{{ nextcloud_spreed_stun_servers_b64encoded['content'] | b64decode | trim }}"

    - name: Set the spreed stun_servers configuration
      ansible.builtin.command: '{{ nextcloud_php_alias }} occ config:app:set spreed stun_servers --value="{{ nextcloud_spreed_stun_servers }}"'
      args:
        chdir: "{{ nextcloud_docroot }}"
      become: true
      become_user: "{{ nextcloud_user }}"

  tags:
    - nextcloud
...
